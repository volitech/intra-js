# Intra.js
A node.js API library for the Epitech intranet.

# Installation
To install this module via npm, simply run:
```bash
npm i https://gitlab.com/volitech/intra-js.git
```

You can then require the module and begin using it:
```javascript
const {Client} = require("intra-js");

const client = new Client();
```

# Documentation
For a list of functionalities and usage examples, see [the wiki](../../wikis).

### Summary
- [Client](../../wikis/client) - the Client class
- [User](../../wikis/user) - a single user
- [Module](../../wikis/module) - a module (ex: B-CPE-110)
  - [Activity](../../wikis/activity) - an activity (ex: BSQ)
    - [Project](../../wikis/project) - a project for that activity
      - [File](../../wikis/file) - a file (ex: B-CPE-110_BSQ.pdf, maps-intermediate.tgz, ...)
      - [Group](../../wikis/group) - a group registered for that project
    - [Appointments](../../wikis/appointments) – appointments grouping multiple slots (ex: BSQ follow-up)
      - [AppointmentBlock](../../wikis/appointment-block) - a block (group) of appointment slots (ex: Monday 21, 9:00 to 11:00)
        - [Slot](../../wikis/slot) - a precise slot (ex: Monday 21 at 10:00, free/taken by group X)
    - [Event](../../wikis/event) - An event for an activity (ex: BSQ Bootstrap)
- [Notification](../../wikis/notification)

# Author
@volifter
