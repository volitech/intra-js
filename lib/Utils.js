class Utils {
    static arrayToObject(array, key_name = "id") {
        return array
            ? Object.fromEntries(array.map(e => [e[key_name], e]))
            : null;
    }

    static parseDuration(time_str) {
        return time_str
            .split(":")
            .reverse()
            .map((n, i) => 60 ** i * n)
            .reduce((a, b) => a + b)
        * 1e3;
    }

    static getDate(date) {
        return date ? new Date(date) : null;
    }
}

module.exports = Utils;
