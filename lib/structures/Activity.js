const Utils         = require("../Utils");

const Event         = require("./Event");
const Project       = require("./Project");
const Appointments  = require("./Appointments");

class Activity {
    constructor(client, data) {
        this.client                 = client;

        this.code                   = data.code || data.codeacti;
        this.year                   = data.year;
        this.moduleCode             = data.module_code;
        this.instanceCode           = data.instance_code;
        this.title                  = data.title;
        this.description            = data.description || "";
        this.location               = data.instance_location || "";
        this.moduleTitle            = data.module_title || "";
        this.typeTitle              = data.type_title || "";
        this.type                   = data.type_code || null;
        this.startDate              = Utils.getDate(data.start);
        this.beginningDate          = Utils.getDate(data.begin);
        this.registrationEndDate    = Utils.getDate(data.end_register);
        this.deadlineDate           = Utils.getDate(data.deadline);
        this.endDate                = Utils.getDate(data.end);
        this.duration               = Utils.parseDuration(data.nb_hours || "");
        this.groupsCount            = +data.nb_group || 0;
        this.isProject              = +data.is_projet || 0;
        this.projectId              = +data.id_projet || null;
        this.project_title          = data.project_title;
        this.hasNotes               = !!data.is_note;
        this.notesCount             = +data.nb_notes || null;
        this.appointmentStatus      = data.rdv_status || null;
        this.gradingScaleId         = +data.id_bareme || null;
        this.gradingScaleTitle      = data.title_bareme || null;
        this.isArchived             = +data.archive || null;
        this.isHidden               = !!data.hidden;
        this.project                = data.project;
        this.events                 = Utils.arrayToObject(
            (data.events || []).map(
                opts => new Event(
                    this.client,
                    Object.assign(
                        opts,
                        {year: this.year},
                        {module_code: this.moduleCode},
                        {instance_code: this.instanceCode},
                        {activity_code: this.code}
                    )
                )
            ),
            "code"
        );
    }

    toString() {
        return `Activity ${this.title} (${this.login})`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            this.code,
            ...route
        ].map(encodeURIComponent).join("/");
    }

    async getAppointments() {
        var data = await this.client.request(this.getURL("rdv"));

        return new Appointments(this.client, data);
    }

    async getProject() {
        if (!this.project) {
            var result = await this.client.request(this.getURL("project"));

            if (result.error)
                return result;
            this.project = new Project(this.client, result);
        }
        return this.project;
    }
}

module.exports = Activity;
