const Utils = require("../Utils");

const Slot  = require("./Slot");

class AppointmentBlock {
    constructor(client, data) {
        this.client         = client;

        this.id             = +data.id;
        this.year           = data.year || null;
        this.moduleCode     = data.module_code || null;
        this.instanceCode   = data.instance_code || null;
        this.activityCode   = data.activity_code || null;
        this.eventCode      = data.codeevent || null;
        this.title          = data.title || null;
        this.type           = data.bloc_status || null;
        this.room           = data.room || null;
        this.slots          = Utils.arrayToObject(
            (data.slots || []).map(
                data => new Slot(
                    this.client,
                    Object.assign(
                        data,
                        {year: this.year},
                        {module_code: this.moduleCode},
                        {instance_code: this.instanceCode},
                        {activity_code: this.activityCode}
                    )
                )
            )
        );
    }

    toString() {
        return `AppointmentBlock #${this.id} (room ${this.room})`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            ...route
        ].map(encodeURIComponent).join("/");
    }
}

module.exports = AppointmentBlock;
