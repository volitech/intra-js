const Utils             = require("../Utils");

const AppointmentBlock  = require("./AppointmentBlock");

class Appointments {
    constructor(client, data) {
        this.client             = client;

        this.year               = data.scolaryear || null;
        this.moduleCode         = data.codemodule || null;
        this.instanceCode       = data.codeinstance || null;
        this.activityCode       = data.codeacti || null;
        this.group              = data.group || null;
        this.title              = data.title || null;
        this.description        = data.description || null;
        this.location           = data.instance_location || null;
        this.moduleTitle        = data.module_title || null;
        this.project            = data.project || null;
        this.isSelfRegistered   = !!data.student_registered;
        this.registeredCount    = +data.nb_registered || null;
        this.appointmentBlocks  = Utils.arrayToObject((data.slots || []).map(
            data => new AppointmentBlock(
                this.client,
                Object.assign(
                    data,
                    {year: this.year},
                    {module_code: this.moduleCode},
                    {instance_code: this.instanceCode},
                    {activity_code: this.activityCode}
                )
            )
        ));
    }

    toString() {
        return `Appointments ${this.title} (${this.moduleTitle})`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            "rdv",
            ...route
        ].map(encodeURIComponent).join("/");
    }
}

module.exports = Appointments;
