const request       = require("request");

const Utils         = require("../Utils");

const Module        = require("./Module");
const Activity      = require("./Activity");
const Event         = require("./Event");
const PlanningEvent = require("./PlanningEvent");
const Notification  = require("./Notification");
const User          = require("./User");

const DEFAULT_CONSTRUCTOR_OPTIONS = {
    base_url: "https://intra.epitech.eu/"
};
const DEFAULT_AUTH_OPTIONS = {
    autologin:  null,
    session_id: null
};
const USER_AGENT = "Intra.js";

class Client {
    constructor(options = {}) {
        options = Object.assign(
            {...DEFAULT_CONSTRUCTOR_OPTIONS, ...DEFAULT_AUTH_OPTIONS},
            options
        );
        this._baseUrl   = options.base_url;
        this._autologin = options.autologin;
        this._sessionId = options.session_id;
        this.selfUser   = null;
    }

    auth(options = {}) {
        options = Object.assign({...DEFAULT_AUTH_OPTIONS}, options);

        this._autologin = options.autologin || this._autologin;
        this._sessionId = options.session_id || this._sessionId;

        return new Promise((resolve, reject) => {
            if (this._sessionId)
                return this.getWhoami().then(resolve, reject);
            if (!this._autologin)
                return reject({
                    error: 'either "session_id" or "autologin" must be set'
                });
            this.request(
                "auth-" + encodeURIComponent(
                    this._autologin.split(this._baseUrl + "auth-").reverse()[0]
                ),
                "GET",
                {},
                {followRedirect: false}
            ).then(_ => {
                this.getWhoami().then(resolve, reject);
            }, reject);
        });
    }

    getWhoami() {
        return new Promise((resolve, reject) => {
            this.request("user").then(data => {
                this.selfUser = new User(this, data);
                resolve(this.selfUser);
            }, reject);
        })
    }

    _setSessionIdByHeaders(headers) {
        if (!headers || !headers["set-cookie"])
            return;
        headers["set-cookie"].forEach(line => {
            line.split("; ").forEach(set_cookie => {
                if (
                    set_cookie.startsWith("user=")
                    && !set_cookie.startsWith("user=deleted")
                )
                    this._sessionId = set_cookie.split("user=")[1];
            });
        });
    }

    request(route, method = "GET", data = {}, options = {}) {
        if (!route.startsWith("auth"))
            data.format = "json";

        let body    = method == "POST" ? data : {};
        let qs      = method == "POST" ? {} : data;

        options = Object.assign({
            url:            this._baseUrl + route,
            method:         method,
            qs:             qs,
            body:           body,
            json:           true,
            headers: {
                "Content-Type": "application/json",
                "User-Agent":   USER_AGENT,
                "Cookie":       `user=${encodeURIComponent(this._sessionId)}`
            }
        }, options);
        return new Promise((resolve, reject) => {
            request(options, (err, res, body) => {
                // console.debug(err, body, (res || {}).statusCode);
                if (!res)
                    return reject({error: "internal error"});
                if ((body || {}).error)
                    return reject(body);
                if (res.statusCode >= 400 || err)
                    return reject({
                        status: res.statusCode,
                        error: (err || {}).msg || err
                    });
                this._setSessionIdByHeaders((res || {}).headers);
                resolve(body);
            });
        });
    }

    getModulesByTime(from, to) {
        return new Promise((resolve, reject) => {
            if (!from || !to)
                return reject({error: "missing date span"});
            this.request("module/board", "GET", {
                start:  new Date(from).toISOString().substr(0, 10),
                end:    new Date(to).toISOString().substr(0, 10)
            }).then(data => {
                resolve(Utils.arrayToObject(
                    data.map(opts => new Module(this, opts))
                ));
            }, reject);
        });
    }

    getModulesByFilter(filter = {}) {
        return new Promise((resolve, reject) => {
            this.request(
                "course/filter",
                "GET",
                Object.assign(
                    {},
                    filter.location && {location: filter.location},
                    filter.year && {scolaryear: filter.year}
                )
            ).then(data => {
                resolve(Utils.arrayToObject(
                    data.map(opts => new Module(this, opts))
                ));
            }, reject);
        });
    }

    getModule(year, module_code, instance_code) {
        return new Promise((resolve, reject) => {
            this.request(
                "module/" + [
                    year,
                    module_code,
                    instance_code
                ].map(encodeURIComponent).join("/")
            ).then(data => {
                resolve(new Module(
                    this,
                    Object.assign(
                        data,
                        {year},
                        {code: module_code},
                        {instance_code}
                    )
                ));
            }, reject);
        });
    }

    getPlanningEvents(start_date, end_date) {

        return new Promise((resolve, reject) => {
            this.request(
                "planning/load/",
                "GET",
                {
                    start: new Date(start_date).toISOString().substr(0, 10),
                    end: new Date(end_date).toISOString().substr(0, 10)
                }
            ).then(items => {
                resolve((Array.isArray(items) ? items : []).map(
                    data => new PlanningEvent(this, data))
                );
            }, reject);
        });
    }

    getActivity(year, module_code, instance_code, activity_code) {
        return new Promise((resolve, reject) => {
            this.request(
                "module/" + [
                    year,
                    module_code,
                    instance_code,
                    activity_code
                ].map(encodeURIComponent).join("/")
            ).then(data => {
                resolve(new Activity(
                    this,
                    Object.assign(
                        data,
                        {year},
                        {module_code},
                        {instance_code},
                        {code: activity_code}
                    )
                ));
            }, reject);
        });
    }

    getProject(year, module_code, instance_code, activity_code) {
        return new Promise((resolve, reject) => {
            this.request(
                "module/" + [
                    year,
                    module_code,
                    instance_code,
                    activity_code,
                    "project"
                ].map(encodeURIComponent).join("/")
            ).then(data => {
                resolve(new Event(this, data));
            }, reject);
        });
    }

    getEvent(year, module_code, instance_code, activity_code, event_code) {
        return new Promise((resolve, reject) => {
            this.request(
                "module/" + [
                    year,
                    module_code,
                    instance_code,
                    activity_code,
                    event_code
                ].map(encodeURIComponent).join("/")
            ).then(data => {
                resolve(new Event(
                    this,
                    Object.assign(
                        data,
                        {year},
                        {module_code},
                        {instance_code},
                        {activity_code},
                        {code: event_code}
                    )
                ));
            }, reject);
        });
    }

    async getNotifications() {
        return Utils.arrayToObject((
                await this.request("user/notification/message")
            ).map(
                data => new Notification(this, data)
        ));
    }
}

module.exports = Client;
