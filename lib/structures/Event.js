const Utils = require("../Utils");

const User  = require("./User");

class Event {
    constructor(client, data) {
        this.client             = client;

        this.code               = data.code || data.codeevent;
        this.year               = data.year;
        this.moduleCode         = data.module_code;
        this.instanceCode       = data.instance_code;
        this.activityCode       = data.activity_code;
        this.count              = +data.num_event || 1;
        this.seatsCount         = +data.seats || 0;
        this.title              = data.title || null;
        this.description        = data.description || null;
        this.registeredCount    = +data.nb_inscrits || +data.nb_registered || 0;
        this.begin              = Utils.getDate(data.begin);
        this.end                = Utils.getDate(data.end);
        this.activityId         = +data.id_activite || null;
        this.location           = data.location || null;
        this.duration           = Utils.parseDuration(data.nb_hours || "");
        this.typeTitle          = data.type_title || null;
        this.typeCode           = data.type_code || null;
        this.myStatus           = data.user_status || null;
        this.isExpectingToken   = !!+data.allow_token;
        this.assistants         = Utils.arrayToObject(
            (data.assistants || []).map(
                data => new User(this.client, data)
            ),
            "login"
        );
    }

    toString() {
        return `Event #${this.code} (${this.title || "Untitled"})`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            this.activityCode,
            this.code,
            ...route
        ].map(encodeURIComponent).join("/");
    }

    async enterToken(value, rating = 0, comment = "") {
        return this.client.request(
            this.getURL("token"),
            "POST",
            {token: value, rate: rating, comment}
        )
    }
}

module.exports = Event;
