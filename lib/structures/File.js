const Utils = require("../Utils");

const User  = require("./User");

class File {
    constructor(client, data) {
        this.client     = client;

        this.type       = data.type || null;
        this.slug       = data.slug || null;
        this.title      = data.title || null;
        this.secure     = !!data.secure;
        this.synchro    = !!data.synchro;
        this.archive    = data.archive || null;
        this.language   = data.language || null;
        this.size       = data.size || null;
        this.ctime      = Utils.getDate(data.ctime);
        this.mtime      = Utils.getDate(data.mtime);
        this.mime       = data.mime || null;
        this.isLeaf     = !!data.isLeaf;
        this.isFolder   = !data.noFolder;
        this.rights     = data.rights || null;
        this.fullpath   = data.fullpath
            ? this.client.baseUrl + data.fullpath.substr(1)
            : null;
        this.modifier   = data.modifier
            ? new User(this.client, data.modifier)
            : null;
    }

    toString() {
        return `File #${this.id} @ ${this.date}`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            ...route
        ].map(encodeURIComponent).join("/");
    }
}

module.exports = File;
