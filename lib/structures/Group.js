const Utils = require("../Utils");

const User  = require("./User");

class Group {
    constructor(client, data) {
        this.client     = client;

        this.id         = data.id;
        this.title      = data.title || null;
        this.code       = data.code || null;
        this.note       = data.final_note || null;
        this.repository = data.repository || null;
        this.isClosed   = !!data.closed;
        this.master     = new User(this.client, data.master);
        this.members    = Utils.arrayToObject(
            (data.members || []).map(
                opts => new User(this.client, opts)
            ),
            "login"
        );
    }

    toString() {
        return `Group #${this.id} (${this.code}): ${this.title}`;
    }
}

module.exports = Group;
