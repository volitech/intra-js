const Utils     = require("../Utils");

const User      = require("./User");
const Activity  = require("./Activity");

class Module {
    constructor(client, data) {
        this.client             = client;

        this.id                 = data.id;
        this.year               = +data.scolaryear;
        this.code               = data.code || data.codemodule || null;
        this.instanceCode       = data.codeinstance || null;
        this.title              = data.title || null;
        this.semester           = +data.semester || null;
        this.begin              = Utils.getDate(data.begin);
        this.end                = Utils.getDate(data.end);
        this.end_register       = Utils.getDate(data.end_register);
        this.locationTitle      = data.location_title || null;
        this.instanceLocation   = data.instance_location || null;
        this.flags              = +data.flags || 0;
        this.credits            = +data.credits || 0;
        this.rights             = data.rights || [];
        this.status             = data.status || null;
        this.isActivePromo      = !!+data.active_promo;
        this.isOpen             = !!+data.open;

        this._setDetailedData(data);
    }

    _setDetailedData(data) {
        var create_user = data => new User(this.client, data);

        this.description        = data.description || "";
        this.competence         = data.competence || "";
        this.responsibles       = (data.resp || []).map(create_user);
        this.assistants         = (data.assistant || []).map(create_user);
        this.unitDesigners      = (data.template_resp || []).map(create_user);
        this.canRegister        = !!data.allow_register;
        this.isRegistsred       = !!data.student_registered;
        this.gradeReceived      = data.student_grade || null;
        this.creditsReceived    = data.student_credits || 0;
        this.isMine             = !!data.current_resp;
        this.activities         = Utils.arrayToObject(
            (data.activites || []).map(
                opts => new Activity(
                    this.client,
                    Object.assign(
                        opts,
                        {year: this.year},
                        {module_code: this.code},
                        {instance_code: this.instanceCode}
                    )
                )
            ),
            "code"
        );
        this.extendedDataLoaded = !!this.description;
    }

    toString() {
        return `Module #${this.id} (${this.title})`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.code,
            this.instanceCode,
            ...route
        ].map(encodeURIComponent).join("/")
    }

    async load() {
        var data = await this.client.request(this.getURL());

        this._setDetailedData(data);
        return this;
    }

    async register() {
        return await this.client.request(this.getURL("register"), "POST");
    }

    async unregister() {
        return await this.client.request(this.getURL("unregister"), "POST");
    }
}

module.exports = Module;
