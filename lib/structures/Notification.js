const Utils = require("../Utils");

const User  = require("./User");

class Notification {
    constructor(client, data) {
        this.client     = client;

        this.id         = +data.id;
        this.title      = data.title || null;
        this.content    = data.content || null;
        this.type       = data.class || null;
        this.date       = Utils.getDate(data.date);
        this.user       = data.user ? new User(this.client, data.user) : null;
        this.text       = (this.title + "\n" + this.content)
            .replace(/<a href=".*?">(.*?)<\/a>/gs,"$1");
        this.links      = [...(this.title + "\n" + this.content)
            .matchAll(/<a href="(.*?)">(.*?)<\/a>/gs)].map(e => e[1]);
    }

    toString() {
        return `Notification #${this.id} @ ${this.date}: ${this.title}`;
    }
}

module.exports = Notification;
