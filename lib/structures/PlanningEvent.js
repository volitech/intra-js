const Utils = require("../Utils");

const User  = require("./User");

class PlanningEvent {
    constructor(client, data) {
        this.client             = client;

        this.title              = data.acti_title;
        this.moduleTitle        = data.titlemodule;
        this.semester           = +data.semester;
        this.year               = data.scolaryear;
        this.moduleCode         = data.codemodule;
        this.instanceCode       = data.codeinstance;
        this.activityCode       = data.codeacti;
        this.eventCode          = data.codeevent;

        this.startDate          = new Date(data.start + " GMT+1");
        this.endDate            = new Date(data.end + " GMT+1");
        this.registeredCount    = +data.total_students_registered || 0;
    }

    toString() {
        return `PlanningEvent #${this.eventCode} (${this.title || "Untitled"})`;
    }

    serialize() {
        return {
            title:              this.title,
            module_title:       this.moduleTitle,
            semester:           this.semester,
            year:               this.year,
            module_code:        this.moduleCode,
            instance_code:      this.instanceCode,
            activity_code:      this.activityCode,
            event_code:         this.eventCode,
            start_date:         this.startDate,
            end_date:           this.endDate,
            registered_count:   this.registeredCount
        }
    }
}

module.exports = PlanningEvent;
