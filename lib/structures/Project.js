const Utils = require("../Utils");

const File  = require("./File");
const Group = require("./Group");

class Project {
    constructor(client, data) {
        var create_group = data => new Group(this.client, data);

        this.client                 = client;

        this.year                   = data.scolaryear || null;
        this.moduleCode             = data.codemodule || null;
        this.instanceCode           = data.codeinstance || null;
        this.activityCode           = data.codeacti || null;
        this.location               = data.instance_location || null;
        this.module_title           = data.module_title || null;
        this.activityId             = data.id_activite || null;
        this.projectTitle           = data.project_title || null;
        this.typeTitle              = data.type_title || null;
        this.typeCode               = data.type_code || null;
        this.canRegister            = !!data.register;
        this.minGroupSize           = +data.nb_min || null;
        this.maxGroupSize           = +data.nb_max || null;
        this.beginningDate          = Utils.getDate(data.begin);
        this.endDate                = Utils.getDate(data.end)
        this.registrationEndDate    = Utils.getDate(data.end_register);
        this.deadlineDate           = Utils.getDate(data.deadline);
        this.is_rdv                 = !!data.is_rdv;
        this.title                  = data.title || null;
        this.description            = data.description || "";
        this.isClosed               = !!data.closed;
        this.daysLeft               = data.over || null;
        this.myStatus               = data.user_project_status || null;
        this.rootSlug               = data.root_slug || null;
        this.slug                   = data.slug || null;
        this.call_ihk               = data.call_ihk || null;
        this.notesCount             = data.nb_notes || null;
        this.isSelfGroupMaster      = !!+data.user_project_master;
        this.selfGroupCode          = data.user_project_code || null;
        this.selfGroupCodeTitle     = data.user_project_title || null;
        this.registeredCount        = data.registered_instance || null;
        this.registered             = Utils.arrayToObject(
            (data.registered || []).map(create_group)
        );
        this.notRegistered          = Utils.arrayToObject(
            (data.notregistered || []).map(create_group)
        );
        this.urls                   = data.urls || null;
        this.files                  = null;
    }

    toString() {
        return `Project #${this.id} @ ${this.date}`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            this.activityCode,
            "project",
            ...route
        ].map(encodeURIComponent).join("/");
    }

    async getFiles() {
        if (!this.files)
            this.files = (await this.client.request(
                    this.getURL("file")
                )).map(
                    opts => new File(this.client, opts)
                );
        return this.files;
    }

    register(users, title = "") {
        return this.client.request(
            this.getURL("register"),
            "POST",
            {
                members: users.map(user => user.login),
                title: title,
                force: false
            }
        )
    }

    // TODO: Properly implement unregistration
    unregister(users, title = "") {
        // return this.client.request(
        //     this.getURL("register"),
        //     "POST",
        //     {
        //         members: users.map(user => user.login),
        //         title: title,
        //         force: false,
        //         unregister: users.map(user => user.login)
        //     }
        // )
    }
}

module.exports = Project;
