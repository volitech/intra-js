const Utils = require("../Utils");

const User  = require("./User");

class Slot {
    constructor(client, data) {
        this.client         = client;

        this.id             = +data.id;
        this.year           = data.year || null;
        this.moduleCode     = data.module_code || null;
        this.instanceCode   = data.instance_code || null;
        this.activityCode   = data.activity_code || null;
        this.title          = data.acti_title || "";
        this.date           = Utils.getDate(data.date);
        this.duration       = data.duration * 60e3;
        this.status         = data.status || null;
        this.type           = data.bloc_status || null;
        this.teamId         = data.id_team || null;
        this.groupCode      = data.code || null;
        this.groupTitle     = data.title || null;
        this.dateSubscribed = Utils.getDate(data.date_ins);
        this.moduleTitle    = data.module_title || null;
        this.hasPassed      = !!+data.past;
        this.groupLeader    = data.master
            ? new User(this.client, data.master)
            : null;
        this.groupMembers   = Utils.arrayToObject(
            (data.members || []).map(
                data => new User(this.client, data)
            ),
            "login"
        );
    }

    toString() {
        return `Slot #${this.id} @ ${this.date}`;
    }

    getURL(...route) {
        return [
            "module",
            this.year,
            this.moduleCode,
            this.instanceCode,
            ...route
        ].map(encodeURIComponent).join("/");
    }

    register() {
        return this.client.request(
            this.getURL("register"),
            "POST",
            {id_creneau: this.id}
        )
    }

    unregister() {
        return this.client.request(
            this.getURL("unregister"),
            "POST",
            {
                id_creneau: this.id,
                id_team:    this.teamId,
                login:      this.teamId
                    ? undefined
                    : (this.groupLeader || {}).login,
            }
        )
    }
}

module.exports = Slot;
