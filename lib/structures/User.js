const Utils = require("../Utils");

class User {
    constructor(client, data) {
        this.client             = client;

        this.login              = data.login || null;
        this.title              = data.title || null;
        this.email              = data.internal_email || null;
        this.lastname           = data.lastname || null;
        this.firstname          = data.firstname || null;
        this.extraInfo          = data.userinfo || null;
        this.picture            = data.picture || null;
        this.scholarYear        = data.scolaryear || null;
        this.promo              = +data.promo || null;
        this.semesterId         = +data.semester || null;
        this.location           = data.location || null;
        this.isClosed           = !!data.close || null;
        this.creationDate       = Utils.getDate(data.ctime);
        this.schoolId           = data.school_id || null;
        this.rights             = data.rights || null;
        this.isAdmin            = !!data.admin || null;
        this.isEditable         = !!data.editable || null;
        this.canViewProfiles    = !data.restrictprofiles || null;
        this.groups             = data.groups || [];
        this.creditsCount       = data.credits || null;
        this.GPA                = Object.fromEntries(
            (data.gpa || []).map(gpa => [gpa.cycle, gpa.gpa])
        );
        this.extendedDataLoaded = !!this.firstname;
        this.grades             = null;
    }

    toString() {
        return `User ${this.title} (${this.login})`;
    }

    getURL(...route) {
        return [
            "user",
            this.login,
            ...route
        ].map(encodeURIComponent).join("/");
    }

    get isMe() {
        return this.id && ((this.client || {}).selfUser || {}).id == this.id;
    }

    async getGrades() {
        if (!this.grades)
            this.grades = await this.client.request(this.getURL("notes"));
        return this.grades;
    }
}

module.exports = User;
