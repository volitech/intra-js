const assert = require("assert");

const Client = require("../lib/structures/Client");

const AUTOLOGIN = process.env.INTRA_AUTOLOGIN;
var client;

if (!AUTOLOGIN) {
    console.error("Error: must specify an $INTRA_AUTOLOGIN env var to launch tests");
    process.exit(1);
}

function assertError(prom, error_msg, done) {
    prom.then(() => {
        assert.fail("promise should have been rejected");
    }, err => {
        assert.deepEqual(err, error_msg);
    }).then(done, done);
}

function prepareClient(done) {
    client = new Client({autologin: AUTOLOGIN});

    client.auth().then(
        _ => { done(); },
        err => { done("Auth failed:" + err); },
    );
}

describe("Client", function() {
    this.timeout(5000);

    describe("auth()", () => {
        describe("No fields", () => {
            it("throws an error", done => {
                assertError(
                    new Client().auth({
                        dummy_key: "dummy_val"
                    }),
                    {error: 'either "session_id" or "autologin" must be set'},
                    done
                );
            });
        });
        describe("Invalid session_id", () => {
            it("throws an error", done => {
                assertError(
                    new Client().auth({
                        session_id: "42"
                    }),
                    {error: null, status: 403},
                    done
                );
            });
        });
        describe("Invalid autologin", () => {
            it("throws an error", done => {
                assertError(
                    new Client().auth({
                        autologin: "42"
                    }),
                    {error: null, status: 403},
                    done
                );
            });
        });
        describe("Auth by autologin", () => {
            it("signs the user in", done => {
                new Client().auth({
                    autologin: AUTOLOGIN
                }).then(_ => {
                    done();
                }, err => {
                    assert.fail("Auth failed:" + err);
                }).catch(done);
            });
        });
    });
    describe("request()", () => {
        beforeEach(prepareClient);

        it("displays data in /user", done => {
            client.request("/user").then(result => {
                if (result.login)
                    return done();
                done("result does not contain the login field: " + JSON.stringify(result));
            }, done);
        });
    });
});
